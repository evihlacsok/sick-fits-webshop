// at its simplest, the access control returns a yes or no value depending on the users session

import { permissionsList } from "./schemas/fields";
import { ListAccessArgs } from "./types";

export function isSignedIn({ session }: ListAccessArgs) {
  return !!session;
}

const generatedPermissions = Object.fromEntries(permissionsList.map(permission => [
  permission,
  function({session}: ListAccessArgs) {
    return !!session?.data.role?.[permission];
  }
]));

// permissions check if someone makes a criteria - yes or no
export const permissions = {
  ...generatedPermissions,
  isAwesome({session}: ListAccessArgs) {
    if(session?.data.name.includes('evi')) {
      return true; // they are awesome
    }
    return false;
  }
};

// rule based function
// rules can return a boolean - yes or no, or a filter which limits which products they can CRUD
export const rules = {
  canManageProducts({ session }: ListAccessArgs) {
    // 1. do they have permission of canManageProducts
    if(permissions.canManageProducts({ session })) {
      return true;
    }
    // 2. if not, do they own this item?
    return { user: { id: session.itemId }};
  },
  canOrder({ session }: ListAccessArgs) {
    if (!isSignedIn({session})) {
      return false;
    }
    // 1. do they have permission of canManageCart
    if(permissions.canManageCart({ session })) {
      return true;
    }
    // 2. if not, do they own this item?
    return { user: { id: session.itemId }};
  },
  canManageOrderItems({ session }: ListAccessArgs) {
    // 1. do they have permission of canManageCart
    if(permissions.canManageCart({ session })) {
      return true;
    }
    // 2. if not, do they own this item?
    return { order: { user: { id: session.itemId }}};
  },
  canReadProducts({ session }: ListAccessArgs) {
    if(permissions.canManageProducts({ session })) {
      return true; // they can read everything
    }
    // they should only see available products (based on the status field)
    return { status: 'AVAILABLE'};
  },
  canManageUsers({ session }: ListAccessArgs) {
    if (!isSignedIn({session})) {
      return false;
    }
    if(permissions.canManageUsers({ session })) {
      return true;
    }
    // otherwise they may only update themselves
    return { id: session.itemId };
  },
}