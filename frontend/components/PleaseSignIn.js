import { useUser } from '../components/User';
import SignIn from '../components/SignIn';

export default function({ children }) {
  const me = useUser();
  if (!me) return <SignIn />;
  return children;
}
