import { render, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import wait from 'waait';
import CartCount from '../components/CartCount';

describe('<CartCount/>', () => {
  it('renders', () => {
    render(<CartCount count={10} />);
  });
  it('matches snapshot', () => {
    const { container, debug } = render(<CartCount count={10} />);
    expect(container).toMatchSnapshot(); 
  });
  it('updates via props', async () => {
    const { container, rerender, debug } = render(<CartCount count={11} />);
    // vanilla style
    expect(container.textContent).toBe('11');
    // fancy style
    expect(container).toHaveTextContent('11');

    // update the props
    rerender(<CartCount count="12" />);
    // wait for 400ms (because there is a transition for the cart count change and for a while both the previous and the new numbers appear)
    expect(container.textContent).toBe('1211');
    await wait(400);
    // await screen.findByText('12');
    expect(container.textContent).toBe('12');

    expect(container).toMatchSnapshot();
  });
});
